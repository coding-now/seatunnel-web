/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {defineComponent, PropType, toRefs, ref, watch} from 'vue'
import { useI18n } from 'vue-i18n'
import { NForm, NFormItem, NInput, NRadioGroup, NRadio, NSpace } from 'naive-ui'
import { useTaskConfigModal } from './use-task-config-modal'
import Modal from '@/components/modal'

const props = {
  showModalRef: {
    type: Boolean as PropType<boolean>,
    default: false
  },
  row: {
    type: Object as PropType<any>,
    default: {}
  }
}

const TaskEditConfigModal = defineComponent({
  name: 'TaskEditConfigModal',
  props,
  emits: ['cancelModal', 'confirmModal'],
  setup(props, ctx) {
    const { t } = useI18n()
    const { variables, handleValidate } = useTaskConfigModal(props, ctx)
    const synchronizationForm: any = ref(null)
    console.log("props.row="+props.row)
    const cancelModal = () => {
      variables.model.id = ''
      variables.model.config = ''
      ctx.emit('cancelModal', props.showModalRef)
    }

    const preCancel = () => {
      ctx.emit('cancelModal')
      variables.model.id = ''
      variables.model.config = ''
      variables.model.engineId = ''
    }

    const confirmModal = () => {
      handleValidate()
    }


    const getNextStep = () => {
      if (synchronizationForm.value) {
        synchronizationForm.value.validate(async (valid: any) => {
          if (!valid) {
            variables.model.id = ''
            variables.model.config = ''
            variables.model.engineId = ''
          }
        })
      }
    }

    watch(
        () => props.showModalRef,
        () => {
          variables.model.id = props.row.id
          variables.model.config = props.row.jobConfig
          variables.model.name = props.row.jobDefineName
          variables.model.engineId = props.row.jobEngineId
        }
    )
    return {
      t,
      ...toRefs(variables),
      cancelModal,
      confirmModal,
      preCancel,
      getNextStep,
      synchronizationForm,
    }
  },
  render() {
    const {
      t,
      getNextStep,
      showModalRef,
      preCancel,
    } = this
    return (
      <template>
        <Modal
          title={this.t('project.synchronization_instance.edit_config')+':'+this.model.name +':'+ this.model.id }
          show={showModalRef}
          modelWidth = '1200px'
          style={{ width: '80%',height: '100%' }}
          onCancel={this.cancelModal}
          onConfirm={this.confirmModal}
          confirmLoading={this.saving}
        >
          <NForm model={this.model} rules={this.rules} ref='taskModalFormRef'>
            <NFormItem
                label={this.t(
                    'project.synchronization_instance.engineId'
                )}
                path='engineId'>
              <NInput  type="text"
                  v-model={[this.model.engineId, 'value']}
                  placeholder={this.t('project.synchronization_instance.engineId')}
              />
            </NFormItem>
            <NFormItem
                label={this.t(
                    'project.synchronization_instance.config'
                )}
                path='config'>
              <NInput
                  type="textarea"
                  rows={ 24 }
                  v-model={[this.model.config, 'value']}
                  placeholder={this.t(
                      'project.synchronization_instance.config'
                  )}
              />
            </NFormItem>
          </NForm>
        </Modal>
      </template>
    )
  }
})

export { TaskEditConfigModal }
