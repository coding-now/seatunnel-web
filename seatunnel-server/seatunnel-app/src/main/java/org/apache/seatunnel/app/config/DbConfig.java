package org.apache.seatunnel.app.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;

@Configuration
@ConditionalOnClass(MybatisPlusAutoConfiguration.class)
public class DbConfig {
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(PaginationInnerInterceptor page) {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(page);
        // interceptor.addInnerInterceptor(optimisticLockerInterceptor());
        return interceptor;
    }
    /*@Bean
    public OptimisticLockerInnerInterceptor optimisticLockerInterceptor(){
        return new OptimisticLockerInnerInterceptor();
    }*/

    @Bean
    @ConditionalOnMissingBean(value = PaginationInnerInterceptor.class)
    public PaginationInnerInterceptor paginationInterceptor(DbType dbType) {
        return new PaginationInnerInterceptor(dbType);
    }

    @Bean
    @ConditionalOnMissingBean
    public DbType dbType() {
        return DbType.MYSQL;
    }
}
