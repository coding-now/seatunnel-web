package org.apache.seatunnel.app.config;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.apache.seatunnel.app.dal.entity.JobInstance;
import org.apache.seatunnel.app.dal.mapper.JobInstanceMapper;
import org.apache.seatunnel.app.service.IJobExecutorService;
import org.apache.seatunnel.engine.core.job.JobStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wangbin
 */
@Component
@Slf4j
public class TaskCheckConfig {
    /**
     * 每两个小时执行一次
     */
    @Scheduled(cron = "0 0 0/2 * * ?")
    public void executeCheck() {
        log.info("start-failed-task-check-and-restore...");
        List<JobInstance> fails = instanceMapper.selectList(Wrappers.lambdaQuery(JobInstance.class)
                .eq(JobInstance::getJobStatus, JobStatus.FAILED.name())
                .select(JobInstance::getId,JobInstance::getCreateUserId));
        if(CollectionUtils.isEmpty(fails)){
            return;
        }
        fails.forEach(e->{
            try {
                executorService.jobStore(e.getCreateUserId(), e.getId());
            }catch (Exception ex){
                log.error("restore-err",ex);
            }
        });
    }

    @Resource
    private JobInstanceMapper instanceMapper;
    @Resource
    private IJobExecutorService executorService;

}
